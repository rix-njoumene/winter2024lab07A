

public class SimpleWar{
	
	public static void main (String[] args){
		
		//create deck + shuffle
		Deck drawPile = new Deck();
		drawPile.shuffle();
		
		System.out.println(drawPile);
		//set players points 
		int p1Points = 0;
		int p2Points = 0;
		
		double p1Score = 0.0;
		double p2Score = 0.0;
		
		//GameLoop
		while(drawPile.length() > 0){
			//start of A Round
			
			//print score BEFORE
			System.out.println( "\n" +"Start of the ROUND Score Summary");
			System.out.println( "PLAYER 1 SCORE: " + p1Score );
			System.out.println( "PLAYER 2 SCORE: " + p2Score  );
			
			//draw two cards for each PLAYER
				//p1Card
			Card p1Card = drawPile.drawTopCard();
			System.out.println( "\n" + "the PLAYER 1 as drawn a " + p1Card );
			p1Score = p1Card.calculateScore();
			System.out.println( p1Score );
			
				//p2Card
			Card p2Card = drawPile.drawTopCard();
			System.out.println( "\n" + "the PLAYER 2 as drawn a " + p2Card );
			p2Score = p2Card.calculateScore();
			System.out.println( p2Score );
			
			//WINNER of the Round
			
			if( p1Score >  p2Score){
				p1Points += 1;
			}
			else{
				p2Points += 1;
			}
			
			//print score AFTER
			System.out.println( "\n" +"End of the ROUND Score Summary");
			System.out.println( "PLAYER 1 SCORE: " + p1Points );
			System.out.println( "PLAYER 2 SCORE: " + p2Points  );
		}
		
		//Winning state + message
		String winnerMessage = "\nBravo ";
		if(p1Points > p2Points){
			winnerMessage += "PLAYER 1 ";
		}
		else if (p1Points == p2Points){
			winnerMessage += "It's a TIE! ";
		}
		else{
			winnerMessage += "PLAYER 2 ";
		}
		 winnerMessage += " you WON the Game! \nYou were quite Lucky!";
		
		System.out.println( winnerMessage );
		
	}
	
	
}