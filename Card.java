
public class Card{
	
	private String suit;
	private String value;
	
	//constructor
	public Card(String value, String suit){
		
		this.suit = suit;
		this.value = value;
	}
	
	//getters
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	//toString
	public String toString(){
		return this.value + " of " + this.suit;
	}
	
	public double calculateScore(){
		
		double score = 0;
		
		//calculate RANK values
		//set array of RANKS of Ace to King
		String[] values = new String[]
		{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		for(int i = 0; i<values.length; i++){
			
			if(this.value.equals(values[i])){
				score += i+1.0;
			}
		}
		
		//calculate SUIT values
		//set array of SUITS
		String[] suits = new String[] {"Club", "Diamond", "Spade", "Heart"};
		
		for(int i = 0; i<suits.length; i++){
			
			if(this.suit.equals(suits[i])){
				score += ((1.0+i)/10.0);
			}
		}
		
		//return final score
		return score;
		
		
	}
}