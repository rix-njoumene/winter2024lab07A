import java.util.Random;


public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//constructor
	public Deck(){
		
		this.numberOfCards = 52;
		this.rng = new Random();
		
		this.cards = new Card[52];
		
		//array of values in fields
		String[] suits = new String[] {"Diamond", "Heart", "Club", "Spade"};
		
		//set values of Ace to King
		String[] values = new String[]
		{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		//select a specific suit
		int cardCounter = 0;
		for(int i = 0; i<suits.length; i++){
			String suit = suits[i];
			
			//generate all cards for a specific suit
			for(int t = 0; t<values.length; t++){
				cards[t + cardCounter] = new Card( values[t], suit);
			}
			cardCounter += 13;
		}
	}
	
	//to String()
	public String toString(){
	
		String allAvailableCards = "\nAll Av cards [" + this.numberOfCards + "]:";
		
		for(int i = 0; i<this.numberOfCards; i++){
			allAvailableCards += "\n" + this.cards[i];
		}
		
		return allAvailableCards;
	}
	
	//instance methods
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		Card lastCard = this.cards[this.numberOfCards -1];
		this.numberOfCards -= 1;
		return lastCard;
	}
	
	public void shuffle(){
		
		for(int i = 0; i<this.numberOfCards; i++){
			//store value of the card that will be shuffle
			int indexShuffle = rng.nextInt(this.numberOfCards);
			Card shuffledCard = this.cards[indexShuffle];
			
			//swap card index
			this.cards[indexShuffle] = this.cards[i];
			this.cards[i] = shuffledCard;	
		}
	
	}
}